import { Component, OnInit } from '@angular/core';
import {TokenStorageService} from '../auth/token-storage.service';
import {Router} from "@angular/router";
import * as d3 from "d3";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  info: any;
  role: any;

  constructor(private token: TokenStorageService, private router: Router) { }

  ngOnInit() {
    d3.select('svg').remove();
    this.info = {
      token: this.token.getToken(),
      username: this.token.getUsername(),
      authorities: this.token.getAuthorities()
    };
    this.role = this.token.getAuthorities();
  }
  logout() {
    this.token.signOut();
    window.location.reload();
  }

  login() {
    this.router.navigate(['Login']);
  }

}
