import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UploadComponent} from './upload/upload.component';
import {LoginComponent} from './login/login.component';
import {LandingComponent} from './landing/landing.component';
import {AboutComponent} from './about/about.component';
import {RegisterUserComponent} from './register-user/register-user.component';

const routes: Routes = [
  {path: 'Upload', component: UploadComponent},
  {path: 'Login', component: LoginComponent},
  {path: 'Landing', component: LandingComponent},
  {path: 'About', component: AboutComponent},
  {path: 'Register', component: RegisterUserComponent},
  {path: '', redirectTo: '/Landing', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
