import { Component, OnInit } from '@angular/core';
import * as d3 from "d3";


@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    d3.select('svg').remove();
  }

}
