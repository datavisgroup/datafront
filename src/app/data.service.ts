import { Injectable } from '@angular/core';
import * as d3 from 'd3';
import { HttpClient } from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  // jsonObj: Array<any>;
  // dimensions: Array<any>;
  // stringDimensions: Array<any>;
  // numberDimensions: Array<any>;

  private jsonObj = new BehaviorSubject<Array<any>>(null);
  jsonObj$ = this.jsonObj.asObservable();

  private stringDimensions = new BehaviorSubject<Array<any>>(null);
  stringDimensions$ = this.stringDimensions.asObservable();

  private numberDimensions = new BehaviorSubject<Array<any>>(null);
  numberDimensions$ = this.numberDimensions.asObservable();



  constructor(private http: HttpClient) { }


  setJsonObj(data) {
    this.jsonObj.next(data);
  }
  setStringDimensions(data) {
    this.stringDimensions.next(data);
  }
  setNumberDimensions(data) {
    this.numberDimensions.next(data);
  }

  private rootUrl = 'https://datavisserver-forgiving-dog.apps.pcf.pcfhost.com';

  getUsers(): Observable<any> {
    return this.http.get( this.rootUrl + '/Login');
  }

  registerUser(user: object) {
    return this.http.post( this.rootUrl + '/Register', user);
  }
  postFile(fileToUpload: File) {
    // const endpoint = 'your-destination-url';
    // const formData: FormData = new FormData();
    // formData.append('fileKey', fileToUpload, fileToUpload.name);
    // return this.httpClient
    //   .post(endpoint, formData, { headers: yourHeadersConfig })
    //   .map(() => { return true; })
    //   .catch((e) => this.handleError(e));
  }
}
