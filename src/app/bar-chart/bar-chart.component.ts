import { Component, OnInit, ElementRef, Input
       , OnChanges, ViewChild, ViewEncapsulation
       , HostListener } from '@angular/core';
import * as d3 from 'd3';
import { DataModel } from 'src/app/data/data.model';
import {DataService} from "../data.service";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-bar-chart',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnInit, OnChanges {
  @ViewChild('chart')
  private chartContainer: ElementRef;
  graphDimensions: FormGroup;

  @Input()
  data: DataModel[];
  @Input()
  csv: string;

  margin = {top: 50, right: 200, bottom: 70, left: 200};

  jsonObj: Array<any>;
  stringDimensions: Array<any>;
  numberDimensions: Array<any>;
  categoricalDimension: string;
  continuousDimension: string;
  horizontal = true;

  constructor(private dataService: DataService) {
    this.dataService.jsonObj$.subscribe(data => {
      this.jsonObj = data;
      // if(this.graphDimensions.controls.number.value != null && this.graphDimensions.controls.category.value != null) {
      //   this.graphDimensions.controls.number = null;
      //   this.graphDimensions.controls.category = null;
      // }
      // if(this.jsonObj != null) {
      //   //this.createHorizontalChart();
      //   this.graph();
      // } else {
      //   d3.select('svg').remove();
      // }


    });
    this.dataService.numberDimensions$.subscribe(data => {this.numberDimensions = data; }, () =>{}, () => {console.log(this.numberDimensions)});
    this.dataService.stringDimensions$.subscribe(data => {this.stringDimensions = data; }, () =>{}, () => {console.log(this.stringDimensions)});


  }

  ngOnInit() {
    d3.select('svg').remove();
    this.graphDimensions = new FormGroup(
      {
        category: new FormControl(),
        number: new FormControl()
      }
    );
  }

  graph() {
    if(this.horizontal){
      this.createHorizontalChart();

    }else{
      this.createVerticalChart();
    }

  }
  
  graphRotate() {
    if(this.horizontal){
      this.horizontal = false;
      this.graph();

    }else{
      this.horizontal = true;
      this.graph();

    }
  }


  onResize() {
    this.graph()
    // this.createHorizontalChart();
  }

  ngOnChanges(): void {
    if (!this.data) {
      return;
    }
    this.graph()
    // this.createHorizontalChart();
  }

  private assignCategory() {
    this.categoricalDimension = this.graphDimensions.controls.category.value;
    console.log(this.graphDimensions.controls.number.value);
    if(this.numberDimensions.includes(this.graphDimensions.controls.number.value)) this.graph();


  }
  private assignValue() {
    this.continuousDimension = this.graphDimensions.controls.number.value;
    if(this.stringDimensions.includes(this.graphDimensions.controls.category.value)) this.graph();
  }

   private createVerticalChart(): void {
    d3.select('svg').remove();


    const data = d3.nest<string, number>()
      .key((d) => { return d[this.categoricalDimension]; })
      .rollup((v) => { return d3.sum(v, (d) => { return d[this.continuousDimension]; }); })
      .entries(this.jsonObj);
    console.log(JSON.stringify(data));

     data.sort(function(a, b) {
       return b.value - a.value;
     });

   // const element = this.chartContainer.nativeElement;
   // const contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
   // const contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;
   //
   //  const svg = d3.select(element).append('svg')
   //    .attr('width', element.offsetWidth)
   //    .attr('height', element.offsetHeight);

     //const element = this.chartContainer.nativeElement;
     const contentWidth = 1300 - this.margin.left - this.margin.right;
     const contentHeight = 610 - this.margin.top - this.margin.bottom;

     const svg = d3.select('app-bar-chart').append('svg')
       .attr('width', 1300)
       .attr('height', 610);


    const x = d3
      .scaleBand()
      .rangeRound([0, contentWidth])
      .padding(0.1)
      .domain(data.map(d => d.key));

    const y = d3
      .scaleLinear()
      .rangeRound([contentHeight, 0])
      .domain([0, d3.max(data, d => d.value)]);

    const g = svg.append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

    g.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + contentHeight + ')')
      .call(d3.axisBottom(x));

    g.append('g')
      .attr('class', 'axis axis--y')
      .call(d3.axisLeft(y).ticks(10))
      .append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', 6)
      .attr('dy', '0.71em')
      .attr('text-anchor', 'end')
      .text('Frequency');

     // svg.append("text")      // text label for the x axis
     //   .attr("x", 760 )
     //   .attr("y", 590 )
     //   .style("text-anchor", "middle")
     //   .text(this.categoricalDimension);

     svg.append("text")      // text label for the x axis
       .attr("x", ((contentWidth / 2) + (this.margin.left)) )
       .attr("y", (contentHeight + this.margin.top + (this.margin.bottom / 2)) )
       .style("text-anchor", "middle")
       .text(this.categoricalDimension);

     svg.append("text")
       .attr("transform", "rotate(-90)")
       .attr("y", 0 )
       .attr("x",0 - ((contentHeight / 2) + this.margin.top))
       .attr("dy", "1em")
       .style("text-anchor", "middle")
       .text(this.continuousDimension);

     // svg.append("text")
     //   .attr("transform", "translate(" + (contentWidth / 2) + " ," + (contentHeight + this.margin.bottom) + ")")
     //   .style("text-anchor", "middle")
     //   .text(this.categoricalDimension);

    g.selectAll('.bar')
      .data(data)
      .enter().append('rect')
      .attr('class', 'bar')
      .attr('x', d => x(d.key))
      .attr('y', d => y(d.value))
      .attr('width', x.bandwidth())
      .attr('height', d => contentHeight - y(d.value));
  }

  private createHorizontalChart(): void {

    d3.select('svg').remove();

    var margin = {
      top: 50,
      right: 200,
      bottom: 70,
      left: 200
    };


    var width = 1300 - margin.left - margin.right;
    var height = 610 - margin.top - margin.bottom;

      const data = d3.nest<string, number>()
        .key((d) => { return d[this.categoricalDimension]; })
        .rollup((v) => { return d3.sum(v, (d) => { return d[this.continuousDimension]; }); })
        .entries(this.jsonObj);
      console.log(JSON.stringify(data));

    var svg = d3.select("app-bar-chart").append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var x = d3.scaleLinear()
      .range([0, width]);

    var y = d3.scaleBand()
      .range([height, 0]);


    var yAxis = d3.axisLeft(y);

      data.sort(function(a, b) {
        return a.value - b.value;
      });

      x.domain([0, d3.max(data, function(d) { return d.value; })]);

      y.domain(data.map(function(d) { return d.key; }))
        .paddingInner(0.1);


      svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x));

      svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

    svg.append("text")      // text label for the x axis
      .attr("x", (width / 2) )
      .attr("y", (height + margin.top) )
      .style("text-anchor", "middle")
      .text(this.continuousDimension);

    svg.append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 0 - margin.left)
      .attr("x",0 - (height / 2))
      .attr("dy", "1em")
      .style("text-anchor", "middle")
      .text(this.categoricalDimension);

      svg.selectAll(".bar")
        .data(data)
        .enter().append("rect")
        .attr("class", "bar")
        .attr("x", 0)
        .attr("height", y.bandwidth())
        .attr("y", function(d) { return y(d.key); })
        .attr("width", function(d) { return x(d.value); });
  }

}
