import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';
import {HttpClient} from '@angular/common/http';
import * as d3 from "d3";

export interface Tile {
  cols: number;
  rows: number;
  color: string;
  radius: string;
  cursor: string;
  name: string;
}

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})

export class UploadComponent implements OnInit {
  csv: string;
  jsonObj: Array<any>;
  dimensions: Array<any>;
  stringDimensions: Array<any>;
  numberDimensions: Array<any>;
  pasteData: boolean;
  uploadData: boolean;
  useSample: boolean;
  loadedData: boolean;
  loadButton: boolean;
  activePaste: boolean;
  activeUpload: boolean;
  activeSample: boolean;

  constructor(private dataService: DataService, private http: HttpClient) {
  }

  tiles: Tile[] = [
    {cols: 3, rows: 2, color: 'lightgrey', radius: '5px', cursor: 'pointer', name: 'Cities'},
    {cols: 1, rows: 2, color: 'transparent', radius: '5px', cursor: 'default', name: ''},
    {cols: 3, rows: 2, color: 'lightgrey', radius: '5px', cursor: 'pointer', name: 'Movies'},
    {cols: 1, rows: 2, color: 'transparent', radius: '5px', cursor: 'default', name: ''},
    {cols: 3, rows: 2, color: 'lightgrey', radius: '5px', cursor: 'pointer', name: 'Cars'},

    {cols: 3, rows: 1, color: 'transparent', radius: '5px', cursor: 'default', name: ''},
    {cols: 1, rows: 1, color: 'transparent', radius: '5px', cursor: 'default', name: ''},
    {cols: 3, rows: 1, color: 'transparent', radius: '5px', cursor: 'default', name: ''},
    {cols: 1, rows: 1, color: 'transparent', radius: '5px', cursor: 'default', name: ''},
    {cols: 3, rows: 1, color: 'transparent', radius: '5px', cursor: 'default', name: ''},

    {cols: 3, rows: 2, color: 'lightgrey', radius: '5px', cursor: 'pointer', name: 'Countries'},
    {cols: 1, rows: 2, color: 'transparent', radius: '5px', cursor: 'default', name: ''},
    {cols: 3, rows: 2, color: 'lightgrey', radius: '5px', cursor: 'pointer', name: 'Demographics'},
    {cols: 1, rows: 2, color: 'transparent', radius: '5px', cursor: 'default', name: ''},
    {cols: 3, rows: 2, color: 'lightgrey', radius: '5px', cursor: 'pointer', name: 'Music'},
  ];

  ngOnInit() {
    d3.select('svg').remove();
    this.pasteToggle();
    this.csv = '';
  }

  uploadToggle() {
    d3.select('svg').remove();
    this.uploadData = true;
    this.useSample = false;
    this.pasteData = false;
    this.loadedData = false;
    this.loadButton = false;
    this.activePaste = false;
    this.activeSample = false;
    this.activeUpload = true;
  }
  sampleToggle() {
    d3.select('svg').remove();
    this.useSample = true;
    this.uploadData = false;
    this.pasteData = false;
    this.loadedData = false;
    this.loadButton = false;
    this.activePaste = false;
    this.activeSample = true;
    this.activeUpload = false;
  }
  pasteToggle() {
    d3.select('svg').remove();
    this.csv = '';
    this.useSample = false;
    this.uploadData = false;
    this.pasteData = true;
    this.loadedData = false;
    this.loadButton = false;
    this.activePaste = true;
    this.activeSample = false;
    this.activeUpload = false;
  }

  public grabPastedCSV(pastedData) {
    this.csv = pastedData;
    this.stringToJsonConverter(this.csv);
  }

  public async grabSampleCSV(i) {
    if (i === 0) {
      this.grabCities();
      this.loadButton = true;
    }
    if (i === 2) {
      this.grabMovies();
      this.loadButton = true;
    }
    if (i === 4) {
      this.grabCars();
      this.loadButton = true;
    }
    if (i === 10) {
      this.grabCountries();
      this.loadButton = true;
    }
    if (i === 12) {
      this.grabDemographics();
      this.loadButton = true;
    }
    if (i === 14) {
      this.grabMusic();
      this.loadButton = true;
    }
  }

  public grabMovies() {
    this.http.get('./assets/movies.csv', {responseType: 'text'})
      .subscribe(
        data => {
          this.csv = data;
        },
        error => {
          console.log(error);
        }
        , () => {
          this.stringToJsonConverter(this.csv);
          this.loadedData = true;
          this.useSample = false;
        });
  }

  public grabCars() {
    this.http.get('./assets/cars.csv', {responseType: 'text'})
      .subscribe(
        data => {
          this.csv = data;
        },
        error => {
          console.log(error);
        }
        , () => {
          this.stringToJsonConverter(this.csv);
          this.loadedData = true;
          this.useSample = false;
        });
  }

  public grabCountries() {
    this.http.get('./assets/countries.csv', {responseType: 'text'})
      .subscribe(
        data => {
          this.csv = data;
        },
        error => {
          console.log(error);
        }
        , () => {
          this.stringToJsonConverter(this.csv);
          this.loadedData = true;
          this.useSample = false;
        });
  }

  public grabDemographics() {
    this.http.get('./assets/demographics.csv', {responseType: 'text'})
      .subscribe(
        data => {
          this.csv = data;
        },
        error => {
          console.log(error);
        }
        , () => {
          this.stringToJsonConverter(this.csv);
          this.loadedData = true;
          this.useSample = false;
        });
  }

  public grabCities() {
    this.http.get('./assets/cities.csv', {responseType: 'text'})
      .subscribe(
        data => {
          this.csv = data;
        },
        error => {
          console.log(error);
        }
        , () => {
          this.stringToJsonConverter(this.csv);
          this.loadedData = true;
          this.useSample = false;
        });
  }

  public grabMusic() {
    this.http.get('./assets/music.csv', {responseType: 'text'})
      .subscribe(
        data => {
          this.csv = data;
        },
        error => {
          console.log(error);
        }
        , () => {
          this.stringToJsonConverter(this.csv);
          this.loadedData = true;
          this.useSample = false;
        });
  }

  public stringToJsonConverter(data: string) {
    const csv = require('csvtojson');
    try {
      csv({
        trim: true,
        checkType: true
      }).fromString(data).then( (json) => {
        //console.log(json);
        this.jsonObj =  json;
        this.dataService.setJsonObj(this.jsonObj);
        this.getDimensions(json);
        //console.log(this.dimensions);
        this.getDimensionsTypes(this.dimensions, this.jsonObj);
        //console.log(this.stringDimensions);
        //console.log(this.numberDimensions);
      });
    } catch (e) {
      console.log('Error:', e.stack);
      return [];
    }
  }

  public getDimensions(json: Array<any>) {
    this.dimensions = Object.keys(json[0]);
  }

  public getDimensionsTypes(dimensions: Array<any>, json: Array<any>) {
    const numberDims = [];
    const stringDims = [];
    for (const dim of dimensions) {
      if (isNaN(json[0][dim])) {
        stringDims.push(dim);
      } else {
        numberDims.push(dim);
      }
    }
    this.stringDimensions = stringDims;
    this.dataService.setStringDimensions(this.stringDimensions);
    this.numberDimensions = numberDims;
    this.dataService.setNumberDimensions(this.numberDimensions);
  }


  public async getStringFromFile(file: File) {
    return new Promise<string>((resolve) => {
      const reader = new FileReader();

      reader.onload = (event: any) => {
        const data = event.target.result;
        resolve(data);
      };
      reader.readAsText(file);
    });
  }

  public async caller(file: File) {
    const result = await this.getStringFromFile(file);
    this.stringToJsonConverter(result);
    this.csv = result;
    this.loadedData = true;
    this.uploadData = false;
    this.loadButton = true;

  }

  public handleFileInput(files: FileList)  {

    const file: File = files.item(0);
    this.caller(file);
  }

}


