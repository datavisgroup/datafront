import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';
import {FormControl, FormGroup} from '@angular/forms';
import {AuthService} from '../auth/auth.service';
import {SignUpInfo} from '../auth/signup-info';
import * as d3 from "d3";

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {
  form: any = {};
  signupInfo: SignUpInfo;
  isSignedUp = false;
  isSignUpFailed = false;
  errorMessage = '';
  registerForm: FormGroup;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    d3.select('svg').remove();
    // this.registerForm = new FormGroup({
    //   name: new FormControl(), username: new FormControl(), email: new FormControl(),
    //   password: new FormControl()
    // });
  }

  onSubmit() {
    // const Ob = {
    //   firstName: this.registerForm.controls.firstName.value,
    //   lastName: this.registerForm.controls.lastName.value,
    //   email: this.registerForm.controls.email.value,
    //   password: this.registerForm.controls.password.value,
    //   avatar: this.registerForm.controls.avatar.value
    // };
    //
    // this.registerService.registerUser(Ob).subscribe();
    // this.registerForm.reset();

    console.log(this.form);

    this.signupInfo = new SignUpInfo(
      this.form.name,
      this.form.username,
      this.form.email,
      this.form.password);

    this.authService.signUp(this.signupInfo).subscribe(
      data => {
        console.log(data);
        this.isSignedUp = true;
        this.isSignUpFailed = false;
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
}
