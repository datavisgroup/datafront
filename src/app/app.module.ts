import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatSelectModule} from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { BarChartComponent } from './bar-chart/bar-chart.component';
// import { PapaParseModule } from 'ngx-papaparse';
import { LoginComponent } from './login/login.component';
import {LandingComponent} from './landing/landing.component';
import {UploadComponent} from './upload/upload.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatGridListModule} from '@angular/material/grid-list';
import { AboutComponent } from './about/about.component';
import { RegisterUserComponent } from './register-user/register-user.component';
import {httpInterceptorProviders} from './auth/auth-interceptor';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {MatFormFieldModule} from "@angular/material";


@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    LoginComponent,
    UploadComponent,
    NavbarComponent,
    BarChartComponent,
    AboutComponent,
    RegisterUserComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatGridListModule,
    MatSelectModule,
    BrowserAnimationsModule,
    MatFormFieldModule
  ],
  providers: [httpInterceptorProviders, {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
